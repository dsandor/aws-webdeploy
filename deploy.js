"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const cdk = require("@aws-cdk/core");
const aws_cdk_static_website_1 = require("aws-cdk-static-website");
class MyWebStack extends aws_cdk_static_website_1.StaticWebsiteStack {
    constructor(scope, id) {
        super(scope, id, props);
    }
}
exports.MyWebStack = MyWebStack;
const workingPath = process.cwd();
const pkg = require(`${workingPath}/package.json`);
if (!pkg.webdeploy) {
    console.error('Error, package.json should have a "webdeploy" configuration section.');
    process.exit(2);
}
let props, stackName;
if (process.env['WEBDEPLOY_CONFIG_NAME'] && pkg.webdeploy[process.env['WEBDEPLOY_CONFIG_NAME'] || '']) {
    const configName = process.env['WEBDEPLOY_CONFIG_NAME'];
    console.log('Using web deploy configuration:', configName);
    stackName = pkg.webdeploy[configName].stackName;
    props = {
        websiteDistPath: pkg.webdeploy[configName].distPath || pkg.webdeploy[configName].websiteDistPath,
        deploymentVersion: pkg.webdeploy[configName].deploymentVersion || pkg.version,
        resourcePrefix: pkg.webdeploy[configName].resourcePrefix,
        indexDocument: pkg.webdeploy[configName].indexDocument || 'index.html',
        certificateArn: pkg.webdeploy[configName].certificateArn,
        domainNames: pkg.webdeploy[configName].domainNames,
    };
}
else {
    stackName = pkg.webdeploy.stackName;
    props = {
        websiteDistPath: pkg.webdeploy.distPath || pkg.webdeploy.websiteDistPath,
        deploymentVersion: pkg.webdeploy.deploymentVersion || pkg.version,
        resourcePrefix: pkg.webdeploy.resourcePrefix,
        indexDocument: pkg.webdeploy.indexDocument || 'index.html',
        certificateArn: pkg.webdeploy.certificateArn,
        domainNames: pkg.webdeploy.domainNames,
    };
}
console.log('Using the following properties for deployment:');
console.table(props);
const app = new cdk.App();
new MyWebStack(app, stackName || pkg.name);
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZGVwbG95LmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiZGVwbG95LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7O0FBQUEscUNBQXNDO0FBQ3RDLG1FQUE0RDtBQUU1RCxNQUFhLFVBQVcsU0FBUSwyQ0FBa0I7SUFDaEQsWUFBWSxLQUFjLEVBQUUsRUFBVTtRQUVwQyxLQUFLLENBQUMsS0FBSyxFQUFFLEVBQUUsRUFBRSxLQUFLLENBQUMsQ0FBQztJQUMxQixDQUFDO0NBQ0Y7QUFMRCxnQ0FLQztBQUVELE1BQU0sV0FBVyxHQUFHLE9BQU8sQ0FBQyxHQUFHLEVBQUUsQ0FBQztBQUVsQyxNQUFNLEdBQUcsR0FBRyxPQUFPLENBQUMsR0FBRyxXQUFXLGVBQWUsQ0FBQyxDQUFDO0FBRW5ELElBQUksQ0FBQyxHQUFHLENBQUMsU0FBUyxFQUFFO0lBQ2xCLE9BQU8sQ0FBQyxLQUFLLENBQUMsc0VBQXNFLENBQUMsQ0FBQztJQUN0RixPQUFPLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDO0NBQ2pCO0FBRUQsSUFBSSxLQUFVLEVBQUUsU0FBaUIsQ0FBQztBQUVsQyxJQUFJLE9BQU8sQ0FBQyxHQUFHLENBQUMsdUJBQXVCLENBQUMsSUFBSSxHQUFHLENBQUMsU0FBUyxDQUFDLE9BQU8sQ0FBQyxHQUFHLENBQUMsdUJBQXVCLENBQUMsSUFBSSxFQUFFLENBQUMsRUFBRTtJQUNyRyxNQUFNLFVBQVUsR0FBUSxPQUFPLENBQUMsR0FBRyxDQUFDLHVCQUF1QixDQUFDLENBQUM7SUFDN0QsT0FBTyxDQUFDLEdBQUcsQ0FBQyxpQ0FBaUMsRUFBRSxVQUFVLENBQUMsQ0FBQztJQUMzRCxTQUFTLEdBQUcsR0FBRyxDQUFDLFNBQVMsQ0FBQyxVQUFVLENBQUMsQ0FBQyxTQUFTLENBQUM7SUFFaEQsS0FBSyxHQUFHO1FBQ04sZUFBZSxFQUFFLEdBQUcsQ0FBQyxTQUFTLENBQUMsVUFBVSxDQUFDLENBQUMsUUFBUSxJQUFJLEdBQUcsQ0FBQyxTQUFTLENBQUMsVUFBVSxDQUFDLENBQUMsZUFBZTtRQUNoRyxpQkFBaUIsRUFBRSxHQUFHLENBQUMsU0FBUyxDQUFDLFVBQVUsQ0FBQyxDQUFDLGlCQUFpQixJQUFJLEdBQUcsQ0FBQyxPQUFPO1FBQzdFLGNBQWMsRUFBRSxHQUFHLENBQUMsU0FBUyxDQUFDLFVBQVUsQ0FBQyxDQUFDLGNBQWM7UUFDeEQsYUFBYSxFQUFFLEdBQUcsQ0FBQyxTQUFTLENBQUMsVUFBVSxDQUFDLENBQUMsYUFBYSxJQUFJLFlBQVk7UUFDdEUsY0FBYyxFQUFFLEdBQUcsQ0FBQyxTQUFTLENBQUMsVUFBVSxDQUFDLENBQUMsY0FBYztRQUN4RCxXQUFXLEVBQUUsR0FBRyxDQUFDLFNBQVMsQ0FBQyxVQUFVLENBQUMsQ0FBQyxXQUFXO0tBQ25ELENBQUM7Q0FDSDtLQUFNO0lBQ0wsU0FBUyxHQUFHLEdBQUcsQ0FBQyxTQUFTLENBQUMsU0FBUyxDQUFDO0lBRXBDLEtBQUssR0FBRztRQUNOLGVBQWUsRUFBRSxHQUFHLENBQUMsU0FBUyxDQUFDLFFBQVEsSUFBSSxHQUFHLENBQUMsU0FBUyxDQUFDLGVBQWU7UUFDeEUsaUJBQWlCLEVBQUUsR0FBRyxDQUFDLFNBQVMsQ0FBQyxpQkFBaUIsSUFBSSxHQUFHLENBQUMsT0FBTztRQUNqRSxjQUFjLEVBQUUsR0FBRyxDQUFDLFNBQVMsQ0FBQyxjQUFjO1FBQzVDLGFBQWEsRUFBRSxHQUFHLENBQUMsU0FBUyxDQUFDLGFBQWEsSUFBSSxZQUFZO1FBQzFELGNBQWMsRUFBRSxHQUFHLENBQUMsU0FBUyxDQUFDLGNBQWM7UUFDNUMsV0FBVyxFQUFFLEdBQUcsQ0FBQyxTQUFTLENBQUMsV0FBVztLQUN2QyxDQUFDO0NBQ0g7QUFFRCxPQUFPLENBQUMsR0FBRyxDQUFDLGdEQUFnRCxDQUFDLENBQUM7QUFDOUQsT0FBTyxDQUFDLEtBQUssQ0FBQyxLQUFLLENBQUMsQ0FBQztBQUVyQixNQUFNLEdBQUcsR0FBRyxJQUFJLEdBQUcsQ0FBQyxHQUFHLEVBQUUsQ0FBQztBQUMxQixJQUFJLFVBQVUsQ0FBQyxHQUFHLEVBQUUsU0FBUyxJQUFJLEdBQUcsQ0FBQyxJQUFJLENBQUMsQ0FBQyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCBjZGsgPSByZXF1aXJlKCdAYXdzLWNkay9jb3JlJyk7XG5pbXBvcnQgeyBTdGF0aWNXZWJzaXRlU3RhY2sgfSBmcm9tICdhd3MtY2RrLXN0YXRpYy13ZWJzaXRlJztcblxuZXhwb3J0IGNsYXNzIE15V2ViU3RhY2sgZXh0ZW5kcyBTdGF0aWNXZWJzaXRlU3RhY2sge1xuICBjb25zdHJ1Y3RvcihzY29wZTogY2RrLkFwcCwgaWQ6IHN0cmluZyApIHtcbiAgICBcbiAgICBzdXBlcihzY29wZSwgaWQsIHByb3BzKTtcbiAgfVxufVxuXG5jb25zdCB3b3JraW5nUGF0aCA9IHByb2Nlc3MuY3dkKCk7XG5cbmNvbnN0IHBrZyA9IHJlcXVpcmUoYCR7d29ya2luZ1BhdGh9L3BhY2thZ2UuanNvbmApO1xuXG5pZiAoIXBrZy53ZWJkZXBsb3kpIHtcbiAgY29uc29sZS5lcnJvcignRXJyb3IsIHBhY2thZ2UuanNvbiBzaG91bGQgaGF2ZSBhIFwid2ViZGVwbG95XCIgY29uZmlndXJhdGlvbiBzZWN0aW9uLicpO1xuICBwcm9jZXNzLmV4aXQoMik7XG59XG5cbmxldCBwcm9wczogYW55LCBzdGFja05hbWU6IHN0cmluZztcblxuaWYgKHByb2Nlc3MuZW52WydXRUJERVBMT1lfQ09ORklHX05BTUUnXSAmJiBwa2cud2ViZGVwbG95W3Byb2Nlc3MuZW52WydXRUJERVBMT1lfQ09ORklHX05BTUUnXSB8fCAnJ10pIHtcbiAgY29uc3QgY29uZmlnTmFtZTogYW55ID0gcHJvY2Vzcy5lbnZbJ1dFQkRFUExPWV9DT05GSUdfTkFNRSddO1xuICBjb25zb2xlLmxvZygnVXNpbmcgd2ViIGRlcGxveSBjb25maWd1cmF0aW9uOicsIGNvbmZpZ05hbWUpO1xuICBzdGFja05hbWUgPSBwa2cud2ViZGVwbG95W2NvbmZpZ05hbWVdLnN0YWNrTmFtZTtcblxuICBwcm9wcyA9IHtcbiAgICB3ZWJzaXRlRGlzdFBhdGg6IHBrZy53ZWJkZXBsb3lbY29uZmlnTmFtZV0uZGlzdFBhdGggfHwgcGtnLndlYmRlcGxveVtjb25maWdOYW1lXS53ZWJzaXRlRGlzdFBhdGgsXG4gICAgZGVwbG95bWVudFZlcnNpb246IHBrZy53ZWJkZXBsb3lbY29uZmlnTmFtZV0uZGVwbG95bWVudFZlcnNpb24gfHwgcGtnLnZlcnNpb24sXG4gICAgcmVzb3VyY2VQcmVmaXg6IHBrZy53ZWJkZXBsb3lbY29uZmlnTmFtZV0ucmVzb3VyY2VQcmVmaXgsXG4gICAgaW5kZXhEb2N1bWVudDogcGtnLndlYmRlcGxveVtjb25maWdOYW1lXS5pbmRleERvY3VtZW50IHx8ICdpbmRleC5odG1sJyxcbiAgICBjZXJ0aWZpY2F0ZUFybjogcGtnLndlYmRlcGxveVtjb25maWdOYW1lXS5jZXJ0aWZpY2F0ZUFybixcbiAgICBkb21haW5OYW1lczogcGtnLndlYmRlcGxveVtjb25maWdOYW1lXS5kb21haW5OYW1lcyxcbiAgfTtcbn0gZWxzZSB7XG4gIHN0YWNrTmFtZSA9IHBrZy53ZWJkZXBsb3kuc3RhY2tOYW1lO1xuXG4gIHByb3BzID0ge1xuICAgIHdlYnNpdGVEaXN0UGF0aDogcGtnLndlYmRlcGxveS5kaXN0UGF0aCB8fCBwa2cud2ViZGVwbG95LndlYnNpdGVEaXN0UGF0aCxcbiAgICBkZXBsb3ltZW50VmVyc2lvbjogcGtnLndlYmRlcGxveS5kZXBsb3ltZW50VmVyc2lvbiB8fCBwa2cudmVyc2lvbixcbiAgICByZXNvdXJjZVByZWZpeDogcGtnLndlYmRlcGxveS5yZXNvdXJjZVByZWZpeCxcbiAgICBpbmRleERvY3VtZW50OiBwa2cud2ViZGVwbG95LmluZGV4RG9jdW1lbnQgfHwgJ2luZGV4Lmh0bWwnLFxuICAgIGNlcnRpZmljYXRlQXJuOiBwa2cud2ViZGVwbG95LmNlcnRpZmljYXRlQXJuLFxuICAgIGRvbWFpbk5hbWVzOiBwa2cud2ViZGVwbG95LmRvbWFpbk5hbWVzLFxuICB9O1xufVxuXG5jb25zb2xlLmxvZygnVXNpbmcgdGhlIGZvbGxvd2luZyBwcm9wZXJ0aWVzIGZvciBkZXBsb3ltZW50OicpO1xuY29uc29sZS50YWJsZShwcm9wcyk7XG5cbmNvbnN0IGFwcCA9IG5ldyBjZGsuQXBwKCk7XG5uZXcgTXlXZWJTdGFjayhhcHAsIHN0YWNrTmFtZSB8fCBwa2cubmFtZSk7Il19